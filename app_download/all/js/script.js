

$(function(){
    $.ajax({
        url: DOMAIN.appconfig,
        type: "get",
        dataType:"application/json",
        data: {
            uid: DOMAIN.UID
        },
        success: function(res) {
            console.log( "res" );
        },
        error: function(error) {
            var resdata =$.parseJSON(error.responseText).Data;
            document.getElementById("name").innerHTML =  resdata.AppName;
            $("#img").attr("src", resdata.AppIcon);
            $("#AppAddressIos").attr("onclick", "openTip('"+resdata.AppAddressIos+"');");
            $("#AppAddressAndroid").attr("onclick", "openTip('"+resdata.AppAddressAndroid+"');");
        }
    });
})
