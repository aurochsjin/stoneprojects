import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

// Vue.prototype.$hostUrl = '//10.10.15.66:19001/api'; // dev host
// Vue.prototype.$hostUrl = 'http://10.10.15.202:19001/api'; // dev host

// Vue.prototype.$hostUrl = 'http://jh.0771tq.com/api'; // pro host
// Vue.prototype.$uid = 1;

Vue.prototype.$hostUrl = 'http://666.0771tq.com/api'; // pro host
Vue.prototype.$uid = 23; //广发
// Vue.prototype.$uid = 26; //金钥匙


App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
