
export default {
    fetch(STORAGE_KEY) {
		//undefined is not an object (evaluating 'window.localStorage'); 
		//[Component] Event Handler Error @ pages/home/index#handleEvent
		
        return JSON.parse(window.localStorage.getItem(STORAGE_KEY || '[]'));
		// return JSON.parse(localStorage.getItem(STORAGE_KEY || '[]'));
    },
    save(STORAGE_KEY,item) {
        window.localStorage.setItem(STORAGE_KEY, JSON.stringify(item));
		// localStorage.setItem(STORAGE_KEY, JSON.stringify(item));
    }
}

